output "bucket_name" {
  description = "The name of the created bucket."
  value = aws_s3_bucket.encrypted_bucket.bucket
}

output "bucket_arn" {
  description = "The ARN of the created bucket."
  value = aws_s3_bucket.encrypted_bucket.arn
}

output "dynamodb_table_name" {
  description = "The name of the dynamodb table created."
  value = aws_dynamodb_table.dynamodb-terraform-state-lock.id
}
data "aws_caller_identity" "current" {}

output "caller_arn" {
  value = data.aws_caller_identity.current.arn
}
output "common-tags" {
  value = local.common_tags
}


