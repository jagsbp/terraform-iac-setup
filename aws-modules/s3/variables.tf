variable "bucket_name" {
  description = "The name to use for the encrypted S3 bucket."
  default = ""
}

variable "bucket_policy_template" {
  description = "A template for the policy to apply to the bucket."
  default = ""
}
variable "source_policy_json" {
  description = "A source policy for the bucket, additional statements to enable encryption will be added to the policy"
  default = ""
}

variable "acl" {
  description = "The canned ACL to apply. Defaults to private."
  default = "private"
}

variable "tags" {
  description = "A map of additional tags to set on the bucket."
  type = map(string)
  default = {}
}

variable "allow_destroy_when_objects_present" {
  description = "Whether or not to allow the bucket to be destroyed if it still contains objects (\"yes\" or \"no\")."
  type = string
  default = "no"
}

variable "extra-tags" {
  default = {}
}

variable "region" {
  description = "AWS Region"
  default     = "us-east-1"
}

variable "shared_cred_file" {
  description = "shared file path"
  default     = "~/.aws/credentials"
}

variable "profile" {
  description = "default profile"
  default     = "psb"
}

variable "key_name" {
  description = "keyname"
  default     = "psb-lab-dev-key-pair-1"
}

variable "Name" {
  description = "Name of Instance"
  default  = ""
}

variable "CommitId" {
  description = "Latest Commit Id from Git Log "
  default     = "775d7a0d3f1ad7edc489a7ea78854a8c5f39344e"
}

variable "Product" {
    description = "USPTO Product/Project Name for all Resources as Prefix"
    default = "PaaS"
}

variable "Stack" {
    description = "PSB Middleware"
    default = "Middleware"
}

variable "LastUpdate" {
    description = "Ansible Date"
    default = "Today"
}

variable "LastUpdateBy" {
    description = "Ansible User"
    default = "sthanneeru1"
}

variable "Environment" {
    description = "USPTO Environment"
    default = "DEV"
}

variable "KeepOn" {
    description = "Resouce To Be Available"
    default = "Mo+Tu+We+Th+Fr:08-19/Sa:00-23/Su:00-23"
}

variable "ProductLine" {
    description = "PSB"
    default = "EBPL"
}

variable "BusinessArea" {
    description = "USPTO Business Area"
    default = "Infra"
}

variable "BusinessProduct" {
    description = "USPTO Business Product"
    default = "InfraTest"
}

variable "PPACode" {
    description = "USPTO PPA Code"
    default = "SPAAS0"
}

variable "ComponentName" {
    description = "USPTO Component Name"
    default = "PA-S"
}
variable "hash_key" {
    description = "Hash Key"
    default = "LockID"
}

variable "read_capacity" {
    description = "Read Capacity"
    default = "20"
}

variable "write_capacity" {
    description = "Write Capacity"
    default = "20"
}

variable "dynamolockname" {
    description = "Dynamodb Table name"
    default = "terraform-state-lock-dynamo"
}

