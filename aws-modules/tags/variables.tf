variable "Name" {
  description = "Name of Instance"
  default  = "EC2"
}

variable "CommitId" {
  description = "Latest Commit Id from Git Log "
  type        = string
}

variable "Product" {
    description = "USPTO Product/Project Name for all Resources as Prefix"
    type        = string
}

variable "Stack" {
    description = "Stack under which the resource falls"
    type        = string
}

variable "Environment" {
    description = "Environment"
    type        = string
}
