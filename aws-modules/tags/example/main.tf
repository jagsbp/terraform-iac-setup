#This below code provisions an EC2 reosurce by using "common-tags" module to add requried tags to the EC2 resource.
provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.shared_cred_file}"
  profile                 = "${var.profile}"
} //provider

resource "aws_instance" "server1" {
  count           = "${var.instance_count}"
  ami             = "${var.ami}"
  instance_type   = "${var.instance_type}"
  key_name        = "${var.key_name}"
  subnet_id       = "${var.subnet}"
  security_groups = "${var.securityGroups}"
  tags = module.common-tags.tags
}

module "common-tags" {
    source = "git::https://prod-cicm.uspto.gov/gitlab/psb/terraform.git//aws-modules/UACS-TAGS"

    BusinessArea           = "Infra"
    Name                   = "PaaS-DEV-EC2-1"
    Stack                  = "Middleware"
    PPAProgramCode         = "SPAAS0"
    CommitId               = "775d7a0d3f1ad7edc489a7ea78854a8c5f39344e"
    LastUpdateBy           = "sthanneeru1"
    BusinessProduct        = "InfraTest"
    LastUpdate             = "Today"
    ProductLine            = "EBPL"
    ComponentID            = "PA-S"
    KeepOn                 = "Mo+Tu+We+Th+Fr:00-23/Sa:00-23/Su:00-23"
    Environment            = "DEV"
    Product                = "PaaS"
}
