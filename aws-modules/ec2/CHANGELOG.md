# Change Log

All notable changes to this project will be documented in this file.
a) add tags support to root block device
b) Updated versions & comments in examples
c) update documentation and pin terraform_docs version to avoid future changes
d) align ci-cd static checks to use individual minimum Terraform versions 
e) add ci-cd workflow for pre-commit checks

