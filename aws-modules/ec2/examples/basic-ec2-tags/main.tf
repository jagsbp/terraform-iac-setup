#storing state file in Backend
terraform {
  backend "s3" {
    bucket = "psb-terraform-state-bucket"
    key    = "ec2test.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-state-lock-dynamo"
  }
}

# Launching EC2 Instance
module "ec2_instance" {
  source  = "git::https://prod-cicm.uspto.gov/gitlab/psb/terraform.git//aws-modules/ec2"
  ami                    = "ami-0457db48470150f0d"
  // "please note we need use  AMI which UACS has provided , please check Platform automation team on latest AMI"
  instance_type          = "t2.micro"
  key_name               = "psb-lab-dev-key-pair-1"
  vpc_security_group_ids = ["sg-09f329e505dddb6e8", "sg-0c10aea6a3f6ba167"]
  Environment            = "dev"
  Product                = "http"
  subnet_id              = "subnet-0383101af50ccc089"
  tags                   = module.common-tags.tags
}

# Attaching UACS tags
module "common-tags" {
    source = "git::https://prod-cicm.uspto.gov/gitlab/psb/terraform.git//aws-modules/UACS-TAGS"

    BusinessArea           = "Infra"
    Name                   = "PaaS-DEV-EC2-1"
    Stack                  = "Middleware"
    PPAProgramCode         = "SPAAS0"
    CommitId               = "775d7a0d3f1ad7edc489a7ea78854a8c5f39344e"
    LastUpdateBy           = "sthanneeru1"
    BusinessProduct        = "InfraTest"
    LastUpdate             = "Today"
    ProductLine            = "EBPL"
    ComponentID            = "PA-S"
    KeepOn                 = "Mo+Tu+We+Th+Fr:08-18/Sa:00-23/Su:00-23"
    Environment            = "DEV"
    Product                = "PaaS"
}

