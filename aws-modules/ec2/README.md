# AWS EC2 Instance Terraform module

Terraform module to creates an EC2 instance on AWS.

## Usage

### Single EC2 Instance

```hcl
# Storing state file in Backend
terraform {
  backend "s3" {
    bucket = "psb-terraform-state-bucket"
    key    = "alb.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-state-lock-dynamo"
  }
}

# Launching EC2 Instance
module "ec2_instance" {
  source  = "git::https://prod-cicm.uspto.gov/gitlab/psb/terraform.git//aws-modules/ec2"
  ami                    = "ami-0457db48470150f0d"
  // "please note we need use  AMI which UACS has provided , please check Platform automation team on latest AMI"
  instance_type          = "t2.micro"
  key_name               = "psb-lab-dev-key-pair-1"
  vpc_security_group_ids = ["sg-09f329e505dddb6e8", "sg-0c10aea6a3f6ba167"]
  Environment            = "dev"
  Product                = "http"
  subnet_id              = "subnet-0383101af50ccc089"
}

```

### Multiple EC2 Instance

```hcl
module "ec2_multipe_instance" {
  source  = "git::https://prod-cicm.uspto.gov/gitlab/psb/terraform.git//aws-modules/ec2"

  for_each = toset(["one", "two", "three"])
 
  ami                    = "ami-0457db48470150f0d"
  // "please note we need use  AMI which UACS has provided , please check Platform automation team on latest AMI"
  instance_type          = "t2.micro"
  key_name               = "psb-lab-dev-key-pair-1"
  monitoring             = true
  vpc_security_group_ids = ["sg-09f329e505dddb6e8", "sg-0c10aea6a3f6ba167"]
  subnet_id              = "subnet-0383101af50ccc089"
  tags                   = local.common_tags

}

```
## Architecture diagram
<br><img src="metadata/ec2-tf.png" width="500px">

## Examples

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13.1 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.51 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| aws_instance| resource |
| aws_spot_instance_request | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| ami | ID of AMI to use for the instance | `string` | `""` | no |
| associate_public_ip_address | Whether to associate a public IP address with an instance in a VPC | `bool` | `null` | no |
| availability_zone | AZ to start the instance in | `string` | `null` | no |
| capacity_reservation_specification | Describes an instance's Capacity Reservation targeting option | `any` | `null` | no |
| cpu_core_count | Sets the number of CPU cores for an instance. | `number` | `null` | no |
| cpu_credits | The credit option for CPU usage (unlimited or standard) | `string` | `null` | no |
| cpu_threads_per_core | Sets the number of CPU threads per core for an instance (has no effect unless cpu\_core\_count is also set). | `number` | `null` | no |
| create | Whether to create an instance | `bool` | `true` | no |
| create_spot_instance | Depicts if the instance is a spot instance | `bool` | `false` | no |
| disable_api_termination | If true, enables EC2 Instance Termination Protection | `bool` | `null` | no |
| ebs_block_device | Additional EBS block devices to attach to the instance | `list(map(string))` | `[]` | no |
| ebs_optimized | If true, the launched EC2 instance will be EBS-optimized | `bool` | `null` | no |
| enable_volume_tags | Whether to enable volume tags (if enabled it conflicts with root\_block\_device tags) | `bool` | `true` | no |
| enclave_options_enabled | Whether Nitro Enclaves will be enabled on the instance. Defaults to `false` | `bool` | `null` | no |
| ephemeral_block_device | Customize Ephemeral (also known as Instance Store) volumes on the instance | `list(map(string))` | `[]` | no |
| get_password_data | If true, wait for password data to become available and retrieve it. | `bool` | `null` | no |
| hibernation | If true, the launched EC2 instance will support hibernation | `bool` | `null` | no |
| host_id | ID of a dedicated host that the instance will be assigned to. Use when an instance is to be launched on a specific dedicated host | `string` | `null` | no |
| iam_instance_profile | IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile | `string` | `null` | no |
| instance_initiated_shutdown_behavior | Shutdown behavior for the instance. Amazon defaults this to stop for EBS-backed instances and terminate for instance-store instances. Cannot be set on instance-store instance | `string` | `null` | no |
| instance_type | The type of instance to start | `string` | `"t3.micro"` | no |
| ipv6_address_count | A number of IPv6 addresses to associate with the primary network interface. Amazon EC2 chooses the IPv6 addresses from the range of your subnet | `number` | `null` | no |
| ipv6_addresses | Specify one or more IPv6 addresses from the range of the subnet to associate with the primary network interface | `list(string)` | `null` | no |
| key_name | Key name of the Key Pair to use for the instance; which can be managed using the `aws_key_pair` resource | `string` | `null` | no |
| launch_template | Specifies a Launch Template to configure the instance. Parameters configured on this resource will override the corresponding parameters in the Launch Template | `map(string)` | `null` | no |
| metadata_options | Customize the metadata options of the instance | `map(string)` | `{}` | no |
| monitoring | If true, the launched EC2 instance will have detailed monitoring enabled | `bool` | `false` | no |
| name | Name to be used on EC2 instance created | `string` | `""` | no |
| network_interface | Customize network interfaces to be attached at instance boot time | `list(map(string))` | `[]` | no |
| placement_group | The Placement Group to start the instance in | `string` | `null` | no |
| input_private_ip | Private IP address to associate with the instance in a VPC | `string` | `null` | no |
| root_block_device | Customize details about the root block device of the instance. See Block Devices below for details | `list(any)` | `[]` | no |
| secondary_private_ips | A list of secondary private IPv4 addresses to assign to the instance's primary network interface (eth0) in a VPC. Can only be assigned to the primary network interface (eth0) attached at instance creation, not a pre-existing network interface i.e. referenced in a `network_interface block` | `list(string)` | `null` | no |
| source_dest_check | Controls if traffic is routed to the instance when the destination address does not match the instance. Used for NAT or VPNs. | `bool` | `true` | no |
| spot_block_duration_minutes | The required duration for the Spot instances, in minutes. This value must be a multiple of 60 (60, 120, 180, 240, 300, or 360) | `number` | `null` | no |
| spot_instance_interruption_behavior | Indicates Spot instance behavior when it is interrupted. Valid values are `terminate`, `stop`, or `hibernate` | `string` | `null` | no |
| spot_launch_group | A launch group is a group of spot instances that launch together and terminate together. If left empty instances are launched and terminated individually | `string` | `null` | no |
| spot_price | The maximum price to request on the spot market. Defaults to on-demand price | `string` | `null` | no |
| spot_type | If set to one-time, after the instance is terminated, the spot request will be closed. Default `persistent` | `string` | `null` | no |
| spot_valid_from | The start date and time of the request, in UTC RFC3339 format(for example, YYYY-MM-DDTHH:MM:SSZ) | `string` | `null` | no |
| spot_valid_until | The end date and time of the request, in UTC RFC3339 format(for example, YYYY-MM-DDTHH:MM:SSZ) | `string` | `null` | no |
| spot_wait_for_fulfillment | If set, Terraform will wait for the Spot Request to be fulfilled, and will throw an error if the timeout of 10m is reached | `bool` | `null` | no |
| subnet_id | The VPC Subnet ID to launch in | `string` | `null` | no |
| tags | A mapping of tags to assign to the resource | `map(string)` | `{}` | no |
| tenancy | The tenancy of the instance (if the instance is running in a VPC). Available values: default, dedicated, host. | `string` | `null` | no |
| timeouts | Define maximum timeout for creating, updating, and deleting EC2 instance resources | `map(string)` | `{}` | no |
| user_data | The user data to provide when launching the instance. Do not pass gzip-compressed data via this argument; see user\_data\_base64 instead. | `string` | `null` | no |
|user_data_base64 | Can be used instead of user\_data to pass base64-encoded binary data directly. Use this instead of user\_data whenever the value is not a valid UTF-8 string. For example, gzip-encoded user data must be base64-encoded and passed via this argument to avoid corruption. | `string` | `null` | no |
| volume_tags | A mapping of tags to assign to the devices created by the instance at launch time | `map(string)` | `{}` | no |
| vpc_security_group_ids | A list of security group IDs to associate with | `list(string)` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| arn | The ARN of the instance |
| capacity_reservation_specification | Capacity reservation specification of the instance |
| id | The ID of the instance |
| instance_state | The state of the instance. One of: `pending`, `running`, `shutting-down`, `terminated`, `stopping`, `stopped` |
| outpost_arn | The ARN of the Outpost the instance is assigned to |
| password_data | Base-64 encoded encrypted password data for the instance. Useful for getting the administrator password for instances running Microsoft Windows. This attribute is only exported if `get_password_data` is true |
| primary_network_interface_id | The ID of the instance's primary network interface |
| private_dns | The private DNS name assigned to the instance. Can only be used inside the Amazon EC2, and only available if you've enabled DNS hostnames for your VPC |
| private_ip | The private IP address assigned to the instance. |
| public_dns | The public DNS name assigned to the instance. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC |
| public_ip | The public IP address assigned to the instance, if applicable. NOTE: If you are using an aws\_eip with your instance, you should refer to the EIP's address directly and not use `public_ip` as this field will change after the EIP is attached |
| spot_bid_status | The current bid status of the Spot Instance Request |
| spot_instance_id | The Instance ID (if any) that is currently fulfilling the Spot Instance request |
| spot_request_state | The current request state of the Spot Instance Request |
| tags_all | A map of tags assigned to the resource, including those inherited from the provider default\_tags configuration block |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Authors

platform Automation As A Service (Platform Services Branch) Team.
Please email all queries to PlatformAutomationAsAService@USPTO.GOV
Report Issues @ https://prod-cicm.uspto.gov/gitlab/groups/psb/-/issues

## License

Apache 2 Licensed. See [LICENSE](https://github.com/terraform-aws-modules/terraform-aws-ec2-instance/tree/master/LICENSE) for full details.
